// add
set a[31..0] 00000000000000000000000000000110
set b[31..0] 00000000000000000000000000000011
set cmd[5..0] 000000
check s[31..0] 00000000000000000000000000001001
check N 0
check Z 0
check V 0
check C 0
check enN 0
check enZ 0
check enVC 0

// addcc
set cmd[5..0] 010000
check enN 1
check enZ 1
check enVC 1

// sub
set cmd[5..0] 000100
check s[31..0] 00000000000000000000000000000011
check N 0
check Z 0
check V 0
check C 0
check enN 0
check enZ 0
check enVC 0