#!/bin/bash

# Copie de l'original dans le fichier à transformer
cat -n progc.c>temp.html

# #include #define while do for if else switch case default return => blue
for mot in `cat dicoblue.ccc`;do 
    sed -i "s~ $mot ~ <font color=blue> $mot </font> ~g" temp.html
done

# static void int float char => purple
for mot in `cat dicopurple.ccc`;do 
    sed -i "s~ $mot ~ <font color=purple> $mot </font> ~g" temp.html
done

# /*  */ en vert
sed -i "s~\/\*~<font color=green>\/\*~" temp.html
sed -i "s~\*\/~\*\/</font>~" temp.html

# "string" en jaune
sed -i "s~\"[!\"]*\"~DEBUT\"[!\"]*\"FIN~"

# // en vert
# sed -i 's~\/\/**~DEBUT\/\/**FIN~' temp.html

# Affichage final
cat temp.html