#!/bin/bash
if [ $# -ne 1 ];then
    echo "Usage: $0 fichier.c"
else
    rad=`echo $1|cut -d'.' -f1`
    ext=`echo $1|cut -d'.' -f2`
    echo "$rad $ext"
fi