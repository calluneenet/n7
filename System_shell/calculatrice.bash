#!/bin/bash
if [ $# -ne 3 ]; then	
	echo "Il faut 3 arguments."  
else	
	case $2 in		
		+) res=`expr $1 + $3`;;
		-) res=`expr $1 - $3`;;
		x|\*) res=`expr $1 \* $3`;;		
		/) res=`expr $1 / $3`;;
		*) res="op inconnue";;
	esac	
	echo $res
fi