#!/bin/bash

if [ $# -lt 1 ]; then
	echo "Usage : arbre -f/-d"
else
	for f in *; do
		[ $1 = -f -o \( $1 = -d -a -d "$f" \) ] && echo $2$f	
		if [ -d "$f" ]; then
			cd "$f"
            $HOME/Documents/n7/System_shell/arbre.bash $1 $2\|--
            cd ..	
		fi
	done
fi