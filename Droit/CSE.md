<h1>Droit social</h1>
<h2>CSE (comité social et économique)</h2>
Fusion des IRP (CE, DP(délégués du personnel), CHSCT) depuis les ordonnances Macron. Dépend de la taille de l'entreprise. 

Aujourd'hui toutes les entreprise doivent (depuis le 1er janvier 2020) disposer d'une telle instance. Ils peuvent être assisté de la part des comptables.

<h2>Definition</h2>
Il regroupe les IRP qui existaient jusqu'à lors. C'est une instance qui fait suite a des élections, les représentants du personnel sont des personnes salariées de l'entreprise et pour ceux d'entre qui devaient être renouvelés depuis le 1er janvier 2018, l'entreprise avait l'obligation de mettre en place un CSE. Toutes les entreprises disposant d'au moins 11 salariés. 

Donc si une entreprise a 11 salariés (en équivalent temps plein) et qu'elle n'a pas mis en place un CSE, elle est en infraction.

<h2>Avantages</h2>
<ul>
    <li>CSE traite : <ul>
        <li>Activités sportive, sociale et culturelle. </li>
        <li>Sécurité et conditions de travail.</li>
        </ul></li>
	<li>Chaque année, le CSE va traiter des négociations de salaire.</li>
<li>Défense des salariés en cas de conflit individuel, notamment lors de licenciement.</li></ul>



Les employeurs sont obligés d'organiser les élections du CSE. Il y a des règles dans l'organisation :

<ul>
    <li>Informer les organisations syndicales => protocole d'accord pré électoral (PAP) (modalités des élections)</li>
    <li>Inviter ces organisations syndicales à fournir une liste de candidats</li>
</ul>

<h2>Composition du CSE</h2>
Quelle que soit la taille de l'entreprise, le CSE se compose de :

<ul>
    <li>Un président : l'employeur (désigné de droit) => il met à l'ordre du jour. Il peut donner mandat à quelqu'un ( se faire représenter )</li>
    <li>Secrétaire (+ de 50 salariés) rôle n°2</li>
    <li>Trésorier (+ de 50 salariés)</li>
    <li>CSSCT : Comission santé sécurité condition de travail (obligatoire pour sociétés de plus de 300 salariés)</li>
    <li>Référent pour le harcèlement sexuel <b>et</b> agissement sexiste</li>
</ul>

Le harcèlement moral est identifié dans la santé, et relève donc du CSSCT.

Dans les entreprises de **moins de 50 salariés** on retrouve les compétences de :

<ul>
    <li>Délégués du personnel qui gère les réclamation individuelles et collectives.</li>
    <li>Une compétence en matière de santé et de sécurité</li>
    <li>Des conditions relatives aux licenciement économique collectif mais aussi il est compétent au reclassement du salarié inapte.</li>
    <li>Droit d'alerte</li>



Si un jour, j'ai une difficulté de santé il faut aller voir le médecin du travail, il a le pouvoir.



Dans les entreprises de **+ de 50 salariés** : 

<ul>
    <li>Ce qui a déjà été dit.</li>
    <li>CE et CHSCT => expression collective des salariés sur la gestion, l'évolution économique et financière de l'entreprise, l'organisation du travail, les techniques de production ou encore la formation professionnelle.</li>
</ul>

L'employeur peut demander au CSE de rechercher des manière d'améliorer les conditions de travail.

-> Article L-312 du code du travail.

Le CSE va pouvoir procéder des inspections en matière de santé, sécurité pour déterminer ce que l'on peut améliorer en terme de condition de travail.

Il détient des membres titulaires et suppléant (comme au sport). 
Dans les réunions du CSE on peut faire venir quelqu'un d'extérieur (expert comptable, retraite,..). l'ordre du jour est décidé par le président et le secrétaire. Puis il est transmis aux membres du CSE au moins 3 jours avant la réunion.

Salarié protégé :

> Salarié investi d'un mandat syndical ou autre et bénéficiant à ce titre de mesures particulières en cas de licenciement, notamment l'obligation pour l'employeur d’obtenir l’autorisation préalable de l’inspection du travail (délégué syndical, délégué du personnel, conseiller prud'homal, etc.)

Il est protégé avant et après l'élection.

<h2>Budget</h2>
Le CSE dispose de 0,2% de la masse salariale pour les entreprises de moins de 2000 salariés. 

> La **masse salariale** (MS), dans sa définition courante, est la somme des rémunérations brutes versées aux salariés, hors rémunérations en nature et cotisations patronales sur une année.

Dans le cas ou l'entreprise dispose de plus de 2000 salariés, ce taux est de 0,22%.

A ca il faut ajouter une contribution.



<h2>Réunion</h2>
Le nombre de réunions du CSE est fixé par accord collectif, sans pouvoir être inférieur à **6** par an.

En l'absence d'accord, le CSE se réunit :

- Au moins 1 fois par mois dans les entreprises de moins de 50 salariés
- Au moins 1 fois tous les 2 mois dans les entreprises de moins de 300 salariés
- Au moins 1 fois par mois dans les entreprises de plus de 300 salariés

Les réunions du CSE rassemblent l'employeur ou son représentant et les membres de la délégation du personnel, ou à défaut leurs suppléants.

Les résolutions du CSE sont prises à la majorité des membres présents. Les délibérations du CSE sont consignées dans un procès-verbal établi par le secrétaire du comité.

L'employeur met à la disposition des membres de la délégation du personnel du CSE le local nécessaire pour leur permettre d'accomplir leur mission et, notamment, de se réunir.



