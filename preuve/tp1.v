(* Ouverture d’une section *)
Section CalculPropositionnel.
(* Les règles de la déduction naturelle doivent être ajoutées à Coq. *)
Require Import Naturelle.
(* Déclaration de variables propositionnelles *)
Variable A B C E Y R : Prop.

Theorem Thm00 : A /\ B -> B /\ A.
I_imp H.
I_et.
E_et_d A.
Hyp H.
E_et_g B.
Hyp H.
Qed.
Print Thm00.

Theorem Thm_1 : ((A \/ B) -> C) -> (B -> C).
I_imp H.
I_imp HB.
E_imp (A \/ B).
Hyp H.
I_ou_d.
Hyp HB.
Qed.
Print Thm_1.


Theorem Thm_2 : A -> ~~A.
I_imp HA.
I_non HNA.
E_non A.
Hyp HA.
Hyp HNA.
Qed.

Theorem Thm_3 : (A -> B) -> (~B -> ~A).
I_imp HAimpB.
I_imp HNB.
I_non HA.
E_non B.
E_imp A.
Hyp HAimpB.
Hyp HA.
Hyp HNB.
Qed.

Theorem Thm_4 : ~~A -> A.
I_imp HNNA.
absurde HNA.
E_non (~A).
Hyp HNA.
Hyp HNNA.
Qed.

Theorem Thm_5 : (~B -> ~A) -> (A -> B).
I_imp HNBimpNA.
I_imp HA.
absurde HNB.
E_non A.
Hyp HA.
E_imp (~B).
Hyp HNBimpNA.
Hyp HNB.
Qed.

Theorem Thm_6 : ((E -> (Y \/ R)) /\ (Y -> R)) -> (~R -> ~E).
I_imp H1.
I_imp NR.
I_non HE.
E_non R.
E_imp Y.
E_et_d(E->Y\/R).
Hyp H1.
E_ou Y R.
E_imp E.
E_et_g(Y->R).
Hyp H1.
Hyp HE.
I_imp HY.
Hyp HY.
I_imp HR.
E_antiT.
E_non R.
Hyp HR.
Hyp NR.
Hyp NR.
Qed.


Theorem Thm000 : A /\ B -> B /\ A.
(* introduction implication *)
intro H.
(* élimination conjonction *)
destruct H as (HA,HB). 
(* introduction conjonction *)
split.
(* hypothèse *)
exact HB.
(* hypothèse *)
exact HA.
Qed.

Theorem Coq_E_imp : ((A -> B) /\ A) -> B.
intro H1.
destruct H1 as (HAimpB,HA).
cut A.
exact HAimpB.
exact HA.
Qed.

Theorem Coq_E_et_g : (A /\ B) -> A.
intro H1.
destruct H1 as (HA, HB).
exact HA.
Qed.

Theorem Coq_E_ou : ((A \/ B) /\ (A -> C) /\ (B -> C)) -> C.
intro H1.
destruct H1 as (H1,H2).
destruct H2 as (H2,H3).
elim H1.
exact H2.
exact H3.
Qed.

Theorem Thm_7 : ((E -> (Y \/ R)) /\ (Y -> R)) -> (~R -> ~E).
intro H1.
intro HNR.
destruct H1 as  (H1,H2).
intro HE.
absurd R.
exact HNR.
cut Y.
exact H2.
elim H1.
intro HY.
exact HY.
intro HR.
absurd R.
exact HNR.
exact HR.
exact HE.
Qed.

End CalculPropositionnel.

