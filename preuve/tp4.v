(* Ouverture d’une section *)
Section Induction.
(* Déclaration d’un domaine pour les éléments des listes *)
Variable A : Set.

Inductive liste : Set :=
Nil : liste
| Cons : A -> liste -> liste.

(* Déclaration du nom de la fonction *)
Variable append_spec : liste -> liste -> liste.
(* Spécification du comportement pour Nil *)
Axiom append_Nil : forall (l : liste), append_spec Nil l = l.
(* Spécification du comportement pour Cons *)
Axiom append_Cons : forall (t : A), forall (q l : liste),
append_spec (Cons t q) l = Cons t (append_spec q l).

Theorem append_Nil_right : forall (l : liste), (append_spec l Nil) = l.
induction l.
apply append_Nil.
rewrite append_Cons.
rewrite IHl.
(* rewrite prend la partie gauche d'une equation et la remplace par la partie droite *)
(* apply verifie que c'est la même chose *)
reflexivity.
Qed.

Fixpoint append_impl (l1 l2 : liste) {struct l1} : liste :=
match l1 with
Nil => l2
| (Cons t1 q1) => (Cons t1 (append_impl q1 l2))
end.

Theorem append_associative : forall (l1 l2 l3 : liste),
(append_spec l1 (append_spec l2 l3)) = (append_spec (append_spec l1 l2) l3).
induction l1.
intros.
rewrite append_Nil.
rewrite append_Nil.
reflexivity.
intros.
repeat rewrite append_Cons.
rewrite IHl1.
reflexivity.
Qed.

Theorem append_correctness : forall (l1 l2 : liste),
(append_spec l1 l2) = (append_impl l1 l2).
induction l1.
intro l2.
rewrite append_Nil.
simpl.
reflexivity.
intro l2.
rewrite append_Cons.
simpl. 
(* simpl calcul la fonction *)
rewrite IHl1.
reflexivity.
Qed.