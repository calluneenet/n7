(* TP Logique no 2. Assistant de preuve COQ *)

(* Ouverture d’une section *)
Section CalculPredicats.
(* Définition de 2 domaines pour les prédicats *)
Variable A B : Type.

Theorem Thm_8 : forall (P Q : A -> Prop),
(forall x1 : A, (P x1) /\ (Q x1))
-> (forall x2 : A, (P x2)) /\ (forall x3 : A, (Q x3)).

(* intro signifie l'intro du quelquesoit et aussi de la fleche*)
intro P.
intro Q.
intro Het.

(*introduction du et*)
split.
intro x2.
(*destruct de l'hypothèse et appliqué sur x2*)
(*solution plus rapide 
destruct (Het x2).
exact H. *)

(*solution par étape*)
cut ((P x2) /\ (Q x2)).
intro Hetx2.
destruct Hetx2.
exact H.
apply(Het x2).
intro x3.
destruct (Het x3).
exact H0.
Qed.

(* Formule du second ordre : Quantification du prédicat phi *)
Theorem Thm_9 : forall (P : A -> B -> Prop),
(exists x1 : A, forall y1 : B, (P x1 y1))
-> forall y2 : B, exists x2 : A, (P x2 y2).
intro P.
intro Himp.
intro y2.
destruct Himp as (x1,Hforall).
exists x1.
apply (Hforall y2).
Qed.

(* Formule du second ordre : Quantification des prédicats phi et psi *)
Theorem Thm_10 : forall (P Q : A -> Prop),
(exists x1 : A, (P x1) -> (Q x1))
-> (forall x2 : A, (P x2)) -> exists x3 : A, (Q x3).
intro HA.
intro HQ.
intro HE.
destruct HE.
exists x.
apply H.
apply H0.
Qed.

End CalculPredicats.

