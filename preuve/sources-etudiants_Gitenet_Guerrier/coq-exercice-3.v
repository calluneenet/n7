Require Import Naturelle.
Section Session1_2019_Logique_Exercice_3.

Variable A B : Prop.

Theorem Exercice_3_Naturelle : ((A -> B) -> ((~A) \/ B)).
Proof.
I_imp H.
E_ou A(~A).
TE.
I_imp HA.
I_ou_d.
E_imp A.
Hyp H.
Hyp HA.
I_imp HNA.
I_ou_g.
Hyp HNA.
Qed.

Theorem Exercice_3_Coq : ((A -> B) -> ((~A) \/ B)).
Proof.
intro HAB.
cut (A \/ ~A).
intro HANA.
destruct HANA.
right.
cut A.
exact HAB.
exact H.
left.
exact H.
apply ( classic A).
Qed.

End Session1_2019_Logique_Exercice_3.

