Require Import Naturelle.
Section Session1_2019_Logique_Exercice_2.

Variable A B : Prop.

Theorem Exercice_2_Naturelle : (((~A) /\ (A \/ B)) -> B).
Proof.
I_imp H.
E_ou A B.
E_et_d (~A).
Hyp H.
I_imp HA.
E_antiT.
E_non A.
Hyp HA.
E_et_g (A\/B).
Hyp H.
I_imp HB.
Hyp HB.
Qed.

Theorem Exercice_2_Coq : (((~A) /\ (A \/ B)) -> B).
Proof.
intro HAB.
destruct HAB as (HA,HB).
elim HB.
intro H1B.
absurd A.
exact HA.
exact H1B.
intro.
exact H.
Qed.

End Session1_2019_Logique_Exercice_2.

