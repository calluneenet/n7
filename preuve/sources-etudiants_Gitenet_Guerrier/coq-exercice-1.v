Require Import Naturelle.
Section Session1_2019_Logique_Exercice_1.

Variable A B : Prop.

Theorem Exercice_1_Naturelle : ((A -> B) /\ (A -> (~B)) -> (~A)).
Proof.
I_imp H.
I_non HNA.
I_antiT B.
E_imp A.
E_et_g (A->~B).
Hyp H.
Hyp HNA.
E_imp A.
E_et_d (A->B).
Hyp H.
Hyp HNA.
Qed.

Theorem Exercice_1_Coq : ((A -> B) /\ (A -> (~B)) -> (~A)).
Proof.
intro HAB.
destruct HAB as (H1, H2).
unfold not.
intro H.
absurd B.
cut A.
exact H2.
exact H.
cut A.
exact H1.
exact H.
Qed.

End Session1_2019_Logique_Exercice_1.

