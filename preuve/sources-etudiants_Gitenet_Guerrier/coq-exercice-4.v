Section Session1_2019_Induction_Exercice_4.

(* Déclaration d’un domaine pour les éléments des listes *)
Variable A : Set.

Variable f : A -> A.

Inductive liste : Set :=
Nil
: liste
| Cons : A -> liste -> liste.

(* Déclaration du nom de la fonction append *)
Variable append_spec : liste -> liste -> liste.

(* Spécification du comportement de append pour Nil en premier paramètre *)
Axiom append_Nil : forall (l : liste), append_spec Nil l = l.

(* Spécification du comportement de append pour Cons en premier paramètre *)
Axiom append_Cons : forall (t : A), forall (q l : liste),
   append_spec (Cons t q) l = Cons t (append_spec q l).

(* Spécification du comportement de append pour Nil en second paramètre *)
Axiom append_Nil_right : forall (l : liste), (append_spec l Nil) = l.

(* append est associative à gauche et à droite *)
Axiom append_associative : forall (l1 l2 l3 : liste),
   (append_spec l1 (append_spec l2 l3)) = (append_spec (append_spec l1 l2) l3).

(* Déclaration du nom de la fonction map *)
Variable map_spec : liste -> liste.

(* Spécification du comportement de map pour Nil en paramètre *)
Axiom map_Nil : (map_spec Nil) = Nil.

(* Spécification du comportement de map pour Cons en paramètre *)
Axiom map_Cons : forall (t : A), forall (q : liste),
   map_spec (Cons t q) = Cons (f t) (map_spec q ).

(* map commute avec append *)
Theorem map_append : forall(l1 l2: liste),
(map_spec (append_spec l1 l2)) = (append_spec (map_spec l1) (map_spec l2)).
Proof.
intros.
induction l1.
rewrite map_Nil.
repeat rewrite append_Nil.
reflexivity.
rewrite append_Cons.
repeat rewrite map_Cons.
rewrite append_Cons.
rewrite IHl1.
reflexivity.
Qed.

(* Implantation de la fonction snoc *)
Fixpoint map_impl (l : liste) {struct l} : liste :=
match l with
Nil => l
| (Cons t q) => (Cons (f t) (map_impl q ))
end.

Theorem map_correctness : forall (l : liste),
   (map_spec l) = (map_impl l).
Proof.
intros.
induction l.
rewrite map_Nil.
reflexivity.
rewrite map_Cons.
rewrite IHl.
reflexivity.
Qed.
		     
End Session1_2019_Induction_Exercice_4.
