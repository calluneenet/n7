# Minutes's Meeting
Date: 16/01/2020  
Chair: Rémi Barra   
Minutes: Callune Gitenet  
Attendes: Alexandre Condamine, Mathieu Edouard, Callune Gitenet, Valentin Hou, Awa Tidjani   

# The New Revolution 
After our CEO was ousted, we sold our company to the Chinese and we are now rich with 10 billion euros. It is
now time to use it for a greater purpose: to take control of France.

WARNING: Only the participants of this meeting should be aware of this project. Besides, we suspect that
there is a mole whose purpose is to reveal our intentions so be cautious!

## Welcome + Presentation of the meeting's objectives
Awa : CCO  
Alexandre: CMO   
Valentin: Unionist   
Mathieu: CFO  
Callune: CTO  
Rémi: New CEO  

## Thinking about how to take control of France

Are we going to to a Revolution or a pouch? We choose Revolution by force, because we don't have time for peace ascension.

We need to:
*  Convince people in the army that don't like current government to joint us.
* Corrupt media, politician, soldier, journalist to control Elite Citizen on top of Pyramidal actual power.
* Control internet and social network so they don't give us bad image.
* Discredit actual government to have the crown with us without cost.
* Alexandre have to contact general Pierre Devilier in the army to help us.
* Don't alert the media, so they don't alert the government.
* Recruit independentist (Brittany, Basquanie, Corsica) for free manpower and weapons.

## Deciding what to do once in power (Ideology, actions)

We will:
* Need a charismatic leader of the federation.
* Need a controller for each region of France.
* Give the hope of conquering star and the universe?
* Shared dream with elon musk that need our Ariane space technology.
* Need financial support from people that share our ideology.
* Not leave EU, become the new French EU and take control over EU.
* Call us the Flying Federation (Vs "la république en marche" because walking sucks.).

## Finding the mole in order to avoid the failure of our action
List of clues:
* About Alexander:
    * Awa thinks Alexander is the mole because he talks too much. 
    * Alexander pretends to bluff, he is not really the mole. But what the point of doing that? He is suspicious. 
* Awa: 
    * Alexandre thinks Awa is the mole because she doesn't talk enough. 
    * Alexandre (the fortune teller) protect Awa.
* Valentin:
    * Mathieu thinks Valentin is the mole because a unionist will never accept what we are planing to do. 
* Mathieu:
    * Mathieu said that since he recruited us and trusts us, he can't be the mole.
## Summary + AOB (Any Other Business)
We killed Valentin. He was the mole. Congratulations.